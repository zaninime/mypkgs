{
  description = "A rusty flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs?ref=nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    rust-overlay,
    naersk,
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        localOverlay = final: prev: {
          rust-toolchain = final.rust-bin.fromRustupToolchainFile ./rust-toolchain;

          naersk = final.callPackage naersk {
            cargo = final.rust-toolchain;
            rustc = final.rust-toolchain;
          };
        };

        pkgs = import nixpkgs {
          inherit system;
          overlays = [(import rust-overlay) localOverlay];
        };

        # run this command to create a copy of the rust src for IDEs like IntelliJ
        copy-rust-src = pkgs.writeShellApplication {
          name = "copy-rust-src";
          runtimeInputs = [pkgs.gitMinimal];
          text = ''
            dest="$(git rev-parse --show-toplevel)/target/rust-src"
            rm -rf "$dest"
            mkdir -p "$(dirname "$dest")"
            cp -r "${pkgs.rust-toolchain.availableComponents.rust-src}/lib/rustlib/src/rust/library" "$dest"
            chmod -R a=,u=rwX "$dest"
          '';
        };

        commonInputs = with pkgs; [
          bashInteractive

          # Cargo subcommands to invoke the LLVM tools shipped with the Rust toolchain
          # https://github.com/rust-embedded/cargo-binutils
          cargo-binutils

          # “Zero setup” cross compilation and “cross testing” of Rust crates
          # https://github.com/cross-rs/cross
          cargo-cross

          # Compile Cargo project with zig as linker
          # https://github.com/rust-cross/cargo-zigbuild
          cargo-zigbuild
        ];

        mkShell = args:
          pkgs.mkShell (args
            // {
              buildInputs = commonInputs ++ (args.buildInputs or []);
            });
      in {
        devShells.default = mkShell {
          buildInputs = with pkgs; [
            rust-toolchain
            copy-rust-src
          ];
        };

        devShells.rustup = mkShell {
          buildInputs = with pkgs; [rustup];
        };

        packages.default = pkgs.naersk.buildPackage {
          src = ./.;
        };

        formatter = pkgs.alejandra;
      }
    );
}
