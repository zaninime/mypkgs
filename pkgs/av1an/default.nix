{
  fetchFromGitHub,
  rustPlatform,
  pkg-config,
  makeBinaryWrapper,
  lib,
  stdenv,
  Security,
  nasm,
  # runtime
  ffmpeg-full,
  vapoursynth,
  libvmaf,
  mkvtoolnix-cli,
  # encoders
  libaom,
  libvpx,
  rav1e,
  svt-av1,
  x264_bin,
  x265,
}: let
  encoders = [
    libaom
    libvpx
    rav1e
    svt-av1
    x264_bin
    x265
  ];

  wrapProgramArgs = [
    # make available all encoders, vapoursynth, vapoursynth's python, ffmpeg and mkvmerge
    "--prefix"
    "PATH"
    ":"
    (lib.makeBinPath (encoders ++ [vapoursynth vapoursynth.python3 ffmpeg-full mkvtoolnix-cli]))

    # setup the python path to avoid segmentation fault
    "--prefix"
    "PYTHONPATH"
    ":"
    "${vapoursynth}/${vapoursynth.python3.sitePackages}"
  ];
in
  rustPlatform.buildRustPackage rec {
    pname = "av1an";
    version = "e9d953d2ffe0a9025cf7245cefd49c52da2d0b3b";

    src = fetchFromGitHub {
      owner = "master-of-zen";
      repo = "av1an";
      rev = version;
      hash = "sha256-oCjF9dbyOzeKmgT8gfF+PJvImAx0szaUB0HWUJkOxmM=";
    };

    cargoHash = "sha256-oEXMSzin3Tt3Yd879tCAfb73rMzu1fG3vu9ED1pk+xM=";

    nativeBuildInputs = [pkg-config rustPlatform.bindgenHook nasm makeBinaryWrapper];

    nativeCheckInputs = encoders;

    buildInputs = [ffmpeg-full vapoursynth libvmaf] ++ lib.optionals stdenv.isDarwin [Security];

    postInstall = ''
      wrapProgram $out/bin/av1an ${lib.escapeShellArgs wrapProgramArgs}
    '';
  }
