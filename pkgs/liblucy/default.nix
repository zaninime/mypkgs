{
  stdenv,
  fetchFromGitHub,
  jq,
}:
stdenv.mkDerivation {
  pname = "liblucy";
  version = "unstable-20220422";

  src = fetchFromGitHub {
    owner = "lucydsl";
    repo = "liblucy";
    rev = "9a56afe34db93a42dd7cf9bd1af1a4b2fddd1762";
    hash = "sha256-5SnTwOWczVJ4diYpMY5QuC2CoJe4sVsRTKXKxkk93w0=";
  };

  nativeBuildInputs = [jq];

  makeFlags = ["bin/lucyc"];

  installPhase = ''
    mkdir -p $out/bin
    cp bin/lucyc $out/bin
  '';
}
