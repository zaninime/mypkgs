{
  stdenv,
  fetchFromGitHub,
  cmake,
  pkg-config,
  ninja,
  l-smash,
  dav1d,
  libxml2,
  libvpx,
  ffmpeg,
  xxhash,
  obuparse,
}:
stdenv.mkDerivation rec {
  pname = "L-SMASH-Works";
  version = "1194.0.0.0";

  src = fetchFromGitHub {
    owner = "HomeOfAviSynthPlusEvolution";
    repo = "L-SMASH-Works";
    rev = version;
    hash = "sha256-uOKtI7PGCqai9LP85hIbyI5tilxuE4N9Ttv/itao8Ik=";
  };

  enableParallelBuilding = true;

  nativeBuildInputs = [cmake pkg-config ninja];

  buildInputs = [xxhash obuparse l-smash ffmpeg dav1d libvpx libxml2];

  cmakeFlags = ["-DBUILD_AVS_PLUGIN=OFF" "-DENABLE_MFX=OFF"];
}
