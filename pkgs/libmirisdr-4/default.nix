{
  lib,
  stdenv,
  fetchFromGitHub,
  cmake,
  pkg-config,
  libusb,
}:
stdenv.mkDerivation rec {
  pname = "libmirisdr-4";
  version = "unstable-20220822";

  src = fetchFromGitHub {
    owner = "ericek111";
    repo = "libmirisdr-5";
    rev = "5a1dcc83affdcaffb1df77cdf3cce25d0ad5d614";
    hash = "sha256-X9kFXksr/YnCi+R7/mwG9GSRdfN532R9eGJCFsJxxT4";
  };

  patches = [./0001-rspdx-support.patch];

  nativeBuildInputs = [cmake pkg-config];
  buildInputs = [libusb];

  meta = with lib; {
    description = "Support of Mirics MSi001 + MSi2500 SDR devices";
    homepage = "https://github.com/f4exb/libmirisdr-4";
    license = with licenses; [gpl2];
  };
}
