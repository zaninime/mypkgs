{
  stdenv,
  fetchFromGitHub,
  sd,
}:
stdenv.mkDerivation {
  pname = "obuparse";
  version = "unstable-20231206";

  src = fetchFromGitHub {
    owner = "dwbuiten";
    repo = "obuparse";
    rev = "478adcc872d5a8a19443e87910508e261a0a60ba";
    hash = "sha256-vVI6zVTIvnpv/Eoj8ms7YPN0XvFoc84R4X5yiDRng90=";
  };

  postPatch = ''
    sd "PREFIX=/usr/local" "PREFIX=${placeholder "out"}" Makefile
  '';

  nativeBuildInputs = [sd];

  enableParallelBuilding = true;
}
