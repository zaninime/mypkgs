{
  fetchFromGitHub,
  rust-bin,
  naersk,
  protobuf,
}: let
  src = fetchFromGitHub {
    owner = "chirpstack";
    repo = "chirpstack-udp-forwarder";
    rev = "v4.1.6";
    hash = "sha256-01L0fWLIHSsKMHghuIH0otEnIo7prxP9RZwPmOUT2b4=";
  };

  toolchain = rust-bin.fromRustupToolchainFile "${src}/rust-toolchain.toml";

  naersk' = naersk.override {
    cargo = toolchain;
    rustc = toolchain;
    clippy = toolchain;
  };
in
  naersk'.buildPackage {
    inherit src;

    nativeBuildInputs = [protobuf];
  }
