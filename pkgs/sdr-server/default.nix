{
  lib,
  stdenv,
  cmake,
  pkg-config,
  fetchFromGitHub,
  libconfig,
  rtl-sdr,
  zlib,
  volk,
  check,
  libusb,
}:
stdenv.mkDerivation rec {
  pname = "sdr-server";
  version = "1.1.8";

  src = fetchFromGitHub {
    owner = "dernasherbrezon";
    repo = "sdr-server";
    rev = version;
    hash = "sha256-xPB1EgsDVc7beoCzTvSFpx5MKpvEgksU3usX1cHHldo=";
  };

  patches = [./0001-cmake-fixes.patch];

  nativeBuildInputs = [cmake pkg-config check];
  buildInputs = [libconfig rtl-sdr zlib volk libusb];

  cmakeFlags = [
    "-DCMAKE_INSTALL_SYSCONFDIR=etc"
  ];

  meta = with lib; {
    description = "High performant TCP server for rtl-sdr";
    homepage = "https://github.com/dernasherbrezon/sdr-server";
    license = with licenses; [gpl2];
  };
}
