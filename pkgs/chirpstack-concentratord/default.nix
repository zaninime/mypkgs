{
  fetchFromGitHub,
  rust-bin,
  naersk,
  protobuf,
  sx1302_hal,
  rustPlatform,
}: let
  src = fetchFromGitHub {
    owner = "chirpstack";
    repo = "chirpstack-concentratord";
    rev = "db63166cd4044dff145d32e178ba997e1547b124";
    hash = "sha256-tB0NnRtoNWWUEvNJdAS+w1L4M7Qxqb4yYhQVQsIQ5ds=";
  };

  toolchain = rust-bin.fromRustupToolchainFile "${src}/rust-toolchain.toml";

  naersk' = naersk.override {
    cargo = toolchain;
    rustc = toolchain;
    clippy = toolchain;
  };

  sx1302_hal' = sx1302_hal.overrideAttrs (old: {
    src = fetchFromGitHub {
      owner = "brocaar";
      repo = "sx1302_hal";
      rev = "V2.1.0r7";
      hash = "sha256-Ue3C7XbaCv/d0NIMclPSE+qcQpxw70DVhXlnpFACzhY=";
    };

    # no patches are really necessary
    patches = [];
  });
in
  naersk'.buildPackage {
    pname = "chirpstack-concentratord";
    version = "master-20240328";
    inherit src;

    nativeBuildInputs = [protobuf rustPlatform.bindgenHook];

    buildInputs = [sx1302_hal'];

    cargoBuildOptions = xs: xs ++ ["-p chirpstack-concentratord-sx1302"];
  }
