{
  stdenv,
  fetchFromGitHub,
  makeBinaryWrapper,
  lib,
}:
stdenv.mkDerivation rec {
  pname = "sx1302_hal";
  version = "2.1.0";

  src = fetchFromGitHub {
    owner = "Lora-net";
    repo = "sx1302_hal";
    rev = "V${version}";
    hash = "sha256-coea5Pzsm66+HrTqDVCan0O+n0pBV2RhxCER0aofWgc=";
  };

  patches = [
    ./patches/0001-customizable-reset-script.patch
    ./patches/0002-chip-model-id-pr60.patch
    ./patches/0003-optional-temp-sensor-pr63.patch
    ./patches/0004-nmea-sentence-validation-pr51.patch
  ];

  nativeBuildInputs = [makeBinaryWrapper];

  enableParallelBuilding = true;

  installPhase = ''
    runHook preInstall

    mkdir -p $out/{bin,lib,include}

    cp libloragw/libloragw.a $out/lib/libloragw-sx1302.a
    cp libtools/*.a $out/lib
    cp -r libloragw/inc $out/include/libloragw-sx1302

    cp util_chip_id/chip_id $out/bin/sx1302_chip_id
    cp packet_forwarder/lora_pkt_fwd $out/bin/sx1302_lora_pkt_fwd
    cp util_net_downlink/net_downlink $out/bin/sx1302_net_downlink
    cp util_boot/boot $out/bin/sx1302_boot
    cp util_spectral_scan/spectral_scan $out/bin/sx1302_spectral_scan
    cp tools/reset_lgw.sh $out/bin/sx1302_reset_lgw

    wrapProgram $out/bin/sx1302_lora_pkt_fwd \
      --set-default RESET_SCRIPT_PATH "$out/bin/sx1302_reset_lgw"

    mkdir -p $out/share/sx1302_hal
    cp packet_forwarder/global_conf* $out/share/sx1302_hal/

    runHook postInstall
  '';
}
