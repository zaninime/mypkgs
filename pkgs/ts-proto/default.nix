{
  fetchFromGitHub,
  makeBinaryWrapper,
  nodejs,
  mkYarnPackage,
  jq,
  stdenv,
  lib,
}:
mkYarnPackage rec {
  pname = "ts-proto";
  version = "1.169.1";

  src = fetchFromGitHub {
    owner = "stephenh";
    repo = "ts-proto";
    rev = "v${version}";
    sha256 = "sha256-MBtjY1Yz/f7oz5UqDUpPlw9/QFOy3skcvjoprW2shZE=";
  };

  nativeBuildInputs = [jq makeBinaryWrapper];

  yarnLock = ./yarn.lock;

  configurePhase = ''
    ln -s $node_modules node_modules
    rm -rf .yarnrc.yml
    jq 'del(.packageManager)' package.json > package2.json
    mv package2.json package.json
  '';

  buildPhase = ''
    export HOME=$(mktemp -d)
    yarn --offline build
  '';

  installPhase = ''
    mkdir -p $out/share
    mv build $out/share/ts-proto

    makeWrapper ${nodejs}/bin/node $out/bin/protoc-gen-ts_proto \
      --prefix PATH : ${lib.getBin stdenv.cc.libc}/bin \
      --set NODE_PATH $node_modules \
      --add-flags $out/share/ts-proto/plugin.js
  '';

  distPhase = "true";
}
