{
  lib,
  stdenv,
  cmake,
  fetchFromGitHub,
  soapysdr,
  libmirisdr-4,
}:
stdenv.mkDerivation {
  pname = "SoapyMiri";
  version = "unstable-20221126";

  src = fetchFromGitHub {
    owner = "ericek111";
    repo = "SoapyMiri";
    rev = "73d9aa48974a207bd9013e8a3073c5772fe54549";
    hash = "sha256-676GmAtpIKwR2gp0v/k8BIgT88Zhg22xn8ZJga+l06g=";
  };

  nativeBuildInputs = [cmake];
  buildInputs = [soapysdr libmirisdr-4];

  meta = with lib; {
    description = "SoapySDR driver for the libmirisdr open-source SDRPlay API";
    homepage = "https://github.com/ericek111/SoapyMiri";
    # license = unknown;
  };
}
