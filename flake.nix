{
  description = "Curated collection of packages not found on nixpkgs";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    flake-compat.url = "github:edolstra/flake-compat";
    rust-overlay.url = "github:oxalica/rust-overlay";
    naersk.url = "github:nix-community/naersk";
  };

  outputs = {
    self,
    nixpkgs,
    flake-utils,
    flake-compat,
    rust-overlay,
    naersk,
  }: let
    perSystemOutputs = system: let
      overlay = final: prev: {
        av1an = final.callPackage ./pkgs/av1an {
          inherit (final.darwin.apple_sdk.frameworks) Security;
          vapoursynth = final.vapoursynth.withPlugins [final.l-smash-works final.ffms];
        };
        liblucy = final.callPackage ./pkgs/liblucy {};
        l-smash-works = final.callPackage ./pkgs/l-smash-works {};
        libmirisdr-4 = final.callPackage ./pkgs/libmirisdr-4 {};
        sdr-server = final.callPackage ./pkgs/sdr-server {};
        soapymiri = final.callPackage ./pkgs/soapymiri {};
        ts-proto = final.callPackage ./pkgs/ts-proto {};
        sx1302_hal = final.callPackage ./pkgs/sx1302_hal {};
        chirpstack-concentratord = final.callPackage ./pkgs/chirpstack-concentratord {};
        chirpstack-udp-forwarder = final.callPackage ./pkgs/chirpstack-udp-forwarder {};
        xxhash = final.callPackage ./pkgs/xxhash {};
        obuparse = final.callPackage ./pkgs/obuparse {};

        x264_bin =
          (prev.x264.override {enableShared = false;})
          .overrideAttrs (old: {
            buildInputs = (old.buildInputs or []) ++ [final.ffmpeg final.l-smash];

            outputs = ["out"];
          });
      };
      pkgs = import nixpkgs {
        inherit system;
        overlays = [rust-overlay.overlays.default naersk.overlay overlay];
      };
      pkgNames = nixpkgs.lib.attrNames (overlay {} {});
    in {
      packages = nixpkgs.lib.getAttrs pkgNames pkgs;
    };
  in
    (flake-utils.lib.eachDefaultSystem perSystemOutputs)
    // {
      nixosModules.zbfw = import ./modules/zbfw;

      templates = {
        rust = {
          path = ./templates/rust;
          description = "A rusty flake";
          welcomeText = ''
            Congrats on starting a new Rust project!

            A few things to get you started:

            1. Update `rust-toolchain` with your requirements
            2. Are you building a library? Check `.gitignore`
            3. Run a `nix develop` shell and init your project with `cargo init` (and the necessary flags)
          '';
        };
      };
    };
}
