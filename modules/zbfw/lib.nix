{
  lib,
  selfZone ? "(self)",
  chainPrefix ? "",
}:
with lib; let
  # --- Utils --- #
  sortByPriority = sort (a: b: a.priority < b.priority);
  printChains = concatStringsSep "\n\n";
  wrapInQuotes = a: ''"${a}"'';
  collectRules = collect (x: (x._type or null) == "rule");

  # --- Rule functions --- #

  /*
  Prints a rule with a readable comment.
  */
  printRule = {
    priority,
    comment,
    text,
    ...
  }: ''
    # ${toString priority} : ${comment}
    ${text}
  '';

  /*
  Produces the name and the key of a rule based on its attributes.
  */
  ruleKeyer = {
    priority,
    ingress,
    egress,
    comment,
  }: {
    name = "${toString priority} : ${ingress}->${egress} : ${comment}";
    key = "${ingress}${egress}${toString priority}";
  };

  /*
  Maps the structured configuration rule into a firewall one.
  Supports both single (input) and double depth (forward).
  */
  mkMapper = depth: path: text: let
    onPath = elemAt path;

    ingress = onPath 0;
    egress =
      if depth == 2
      then onPath 1
      else selfZone;
    name =
      if depth == 1
      then onPath 1
      else onPath 2;

    nameParts = builtins.match "([[:digit:]]+): (.+)" name;
    namePart = elemAt nameParts;

    priority = toInt (namePart 0);
    comment = namePart 1;
  in
    {
      _type = "rule";
      inherit text priority comment ingress egress;
    }
    // ruleKeyer {inherit priority ingress egress comment;};

  /*
  Creates one nftables rule containing all the ports to open for the zone.
  */
  mkPortsRule = {
    zone,
    ports,
  }: let
    comment = "nixos managed tcp/udp ports";
    mapEntry = proto: port: "${proto} . ${toString port}";
    mapContent = concatStringsSep ", " (map (mapEntry "tcp") ports.tcp ++ map (mapEntry "udp") ports.udp);
    baseAttrs = {
      _type = "rule";
      priority = 100;
      inherit comment;
      ingress = zone;
      egress = selfZone;
      text = "meta l4proto . th dport { ${mapContent} } accept comment ${wrapInQuotes comment}";
    };
  in
    baseAttrs // ruleKeyer {inherit (baseAttrs) priority ingress egress comment;};
in
  {cfg}: let
    # --- Values --- #
    /*
    All zone names.
    Type: zoneNames :: [zone]
    */
    zoneNames = attrNames cfg.zones;

    /*
    All rules that open ports in input chains.
    Type: openPortsRules :: [rule]
    */
    openPortsRules = pipe zoneNames [
      (filter hasOpenPortsInZone)
      (map (zone: {
        inherit zone;
        ports = openPortsInZone zone;
      }))
      (map mkPortsRule)
    ];

    /*
    A map keyed on interface names, and whose values are the list of zones
    the interface is declared part of. The list should ideally have only one value.
    Type: interfaceToZones :: Map string [string]
    */
    interfaceToZones = pipe cfg.zones [
      (
        mapAttrsToList (
          n: {interfaces, ...}:
            genAttrs interfaces (_: n)
        )
      )
      (foldAttrs (n: v: [n] ++ v) [])
    ];

    /*
    A refinement of `interfaceToZones` that gets rid of the list on the value side.
    Type: interfaceToZone :: Map string string
    */
    interfaceToZone = mapAttrs (_: head) interfaceToZones;

    /*
    Combinations of ingress zone -> egress zone for which there's at least one rule declared.
    Type: forwardingZoneCombinations :: [{ ingress :: string, egress :: string }]
    */
    forwardingZoneCombinations =
      filter (
        {
          ingress,
          egress,
        }:
          ingress != egress && (hasForwardRules ingress egress)
      )
      (
        cartesianProductOfSets {
          ingress = zoneNames;
          egress = zoneNames;
        }
      );

    /*
    Rules pertaining to input packet processing.
    Type: inputRules :: [Rule]
    */
    inputRules = pipe cfg.rules.input [
      (mapAttrsRecursive (mkMapper 1))
      collectRules
      (rules: rules ++ openPortsRules)
      sortByPriority
    ];

    /*
    Rules pertaining to forward packet processing.
    Type: forwardRules :: [Rule]
    */
    forwardRules = pipe cfg.rules.forward [
      (mapAttrsRecursive (mkMapper 2))
      collectRules
      sortByPriority
    ];

    # --- Functions --- #
    /*
    Summarizes the open ports in a zone.
    Type: openPortsInZone :: string -> { tcp :: [number], udp :: [number] }
    */
    openPortsInZone = zone: {
      tcp = cfg.openPorts.tcp."${zone}" or [];
      udp = cfg.openPorts.udp."${zone}" or [];
    };

    # --- Assertions --- #

    /*
    Checks if a zone has any input rules declared.
    Type: hasInputRules :: string -> bool
    */
    hasInputRules = (flip hasAttr) cfg.rules.input;

    /*
    Checks if there are any open ports for the zone.
    Type: hasOpenPortsInZone :: string -> bool
    */
    hasOpenPortsInZone = zone: let p = openPortsInZone zone; in p.tcp != [] || p.udp != [];

    /*
    Checks if there are any rules from the ingress zone to the egress zone.
    Type: hasForwardRules :: string -> string -> bool
    */
    hasForwardRules = ingress: egress:
      (hasAttr ingress cfg.rules.forward)
      && (hasAttr egress cfg.rules.forward."${ingress}");

    /*
    Checks if there's any rule in which the zone appears.
    Type: hasAnyRules :: string -> bool
    */
    hasAnyRules = zone: take 1 (filter (rule: rule.ingress == zone || rule.egress == zone) (inputRules ++ forwardRules)) != [];

    /*
    Checks if the zone has more than one interface attached.
    Type: hasMultipleInterfaces :: string -> bool
    */
    hasMultipleInterfaces = zone: length cfg.zones."${zone}".interfaces > 1;

    # --- Auxiliary chains --- #
    inputChains = let
      relevantRules = zone:
        filter (
          {ingress, ...}:
            ingress == zone
        )
        inputRules;

      mkChain = zone: _: ''
        chain ${chainPrefix}input-${zone} {
        ${concatMapStringsSep "\n" printRule (relevantRules zone)}
        }
      '';

      relevantZones = filterAttrs (n: _: hasInputRules n || hasOpenPortsInZone n) cfg.zones;
    in
      mapAttrsToList mkChain relevantZones;

    forwardChains = let
      relevantRules = ingress': egress':
        filter
        (
          {
            ingress,
            egress,
            ...
          }:
            ingress == ingress' && egress == egress'
        )
        forwardRules;

      mkChain = {
        ingress,
        egress,
      }: ''
        chain ${chainPrefix}forward-${ingress}-${egress} {
        ${concatMapStringsSep "\n" printRule (relevantRules ingress egress)}
        }
      '';
    in
      map mkChain forwardingZoneCombinations;
  in {
    /*
    Rule to be inserted in the input chain.
    It dispatches the packet being processed to the appropriate zone rule.
    Type: dispatchingInputRule :: string
    */
    dispatchingInputRule = let
      verdict = interface: zone: "${wrapInQuotes interface} : jump ${chainPrefix}input-${zone}";

      relevantInterfaceToZone = filterAttrs (const (zone: hasInputRules zone || hasOpenPortsInZone zone)) interfaceToZone;

      content = ''
        iifname vmap { ${
          concatStringsSep ", " (mapAttrsToList verdict relevantInterfaceToZone)
        } }
      '';
    in
      optionalString (relevantInterfaceToZone != {}) content;

    /*
    Rule to be inserted in the forward chain.
    It dispatches the packet being processed to the appropriate zone->zone rule.
    Type: dispatchingForwardRule :: string
    */
    dispatchingForwardRule = let
      mkCrossJumps = {
        ingress,
        egress,
      }: let
        verdict = "jump ${chainPrefix}forward-${ingress}-${egress}";

        interfaceCombinations = cartesianProductOfSets {
          ingressIf = cfg.zones."${ingress}".interfaces;
          egressIf = cfg.zones."${egress}".interfaces;
        };

        mkRule = {
          ingressIf,
          egressIf,
        }: "${wrapInQuotes ingressIf} . ${wrapInQuotes egressIf} : ${verdict}";
      in
        map mkRule interfaceCombinations;

      mkIntraAccept = zone: let
        interfaceCombinations = cartesianProductOfSets {
          ing = cfg.zones."${zone}".interfaces;
          egr = cfg.zones."${zone}".interfaces;
        };

        inherit (cfg.zones."${zone}") createStickRules intraZoneAction;
      in
        pipe interfaceCombinations [
          (filter (c: !createStickRules -> c.ing != c.egr))
          (map (c: "${wrapInQuotes c.ing} . ${wrapInQuotes c.egr} : ${intraZoneAction}"))
        ];

      crossJumps = concatMap mkCrossJumps forwardingZoneCombinations;
      intraAccepts = concatMap mkIntraAccept zoneNames;
      allRules = crossJumps ++ intraAccepts;
    in
      optionalString (allRules != []) ''
        meta iifname . meta oifname vmap { ${concatStringsSep ",\n" allRules} }
      '';

    /*
    Chains to be added to the declarations of the table where the `dispatchingInputRule` and `dispatchingForwardRule` have been used.
    Type: auxiliaryDeclarations :: string
    */
    auxiliaryDeclarations = printChains (inputChains ++ forwardChains);

    /*
    Problems found while processing the configuration.
    */
    problems = {
      /*
      Rules that have a conflicting (aka equal) priority.
      Type: rulesWithConflictingPriorities :: [Rule]
      */
      rulesWithConflictingPriorities = pipe (inputRules ++ forwardRules) [
        (groupBy (getAttr "key"))
        (filterAttrs (_: v: length v > 1))
        attrValues
        flatten
      ];

      /*
      Zone names used in rules but never declared.
      Type :: undefinedZones :: [string]
      */
      undefinedZones = let
        discoveredInput = attrNames cfg.rules.input;
        discoveredForward = pipe cfg.rules.forward [
          (mapAttrsRecursive (p: _: [(elemAt p 0) (elemAt p 1)]))
          (collect isList)
          flatten
        ];
        discoveredWithOpenPorts = attrNames cfg.openPorts.tcp ++ attrNames cfg.openPorts.udp;
        discovered = unique (discoveredInput ++ discoveredForward ++ discoveredWithOpenPorts);
        declared = attrNames cfg.zones;
      in
        subtractLists declared discovered;

      /*
      Interfaces that have been assigned to more than one zone at the same time.
      Type :: interfacesInMultipleZones :: [string]
      */
      interfacesInMultipleZones = filterAttrs (_: v: length v > 1) interfaceToZones;

      /*
      Zones declared and with rules assigned, but without interfaces.
      Type: emptyZones :: [string]
      */
      emptyZones =
        pipe cfg.zones [(filterAttrs (zone: config: config.interfaces == [] && hasAnyRules zone)) attrNames];

      /*
      Zones using a reserved name.
      Type: reservedZoneNames :: [string]
      */
      reservedZoneNames = filter (flip hasAttr cfg.zones) [selfZone];
    };
  }
