{lib, ...}:
with lib; let
  /*
  Same as mkEnableOption from lib, but defaults to true.
  Type: mkEnableOption' :: string -> Option
  */
  mkEnableOption' = name: (mkEnableOption name) // {default = true;};

  /*
  Creates an Option for nftables rules with some common defaults.
  Type: mkRules :: AttrSet -> Option
  */
  mkRules = attrs:
    mkOption ({
        description = "Rules to insert to enable the functionality.";
        type = types.str;
      }
      // attrs);

  fragmentOptions = {
    common = {
      statefulTreatment = {
        enable = mkEnableOption' "Accept established and related flows, drop the invalid ones";
        rules = mkRules {default = "ct state vmap { invalid : drop, established : accept, related : accept }";};
      };
    };

    input = {
      acceptLoopback = {
        enable = mkEnableOption' "Accept packets originating locally";
        rules = mkRules {default = ''meta iiftype loopback accept comment "accept loopback packets"'';};
      };

      ipv6Neighbors = {
        enable = mkEnableOption' "Accept the minimum ICMPv6 packets to enable IPv6 neighbor discovery";
        rules = mkRules {
          default = ''icmpv6 type { nd-neighbor-advert, nd-neighbor-solicit } accept comment "IPv6 neighbor discovery"'';
        };
      };
    };

    forward = {
      offload = {
        enable = mkEnableOption "Offload flows (fast-path)";
        rules = mkRules {}; # default is set in config, as it depends on the other settings
        flowtableName = mkOption {
          description = "The name of the table to offload to";
          type = types.str;
        };
      };

      acceptDNat = {
        enable = mkEnableOption "Accept packets with destination addresses rewritten through DNAT in prerouting";
        rules = mkRules {default = ''ct status dnat accept comment "accept D-NATted packets"'';};
      };
    };
  };

  chainModule = fragmentName: {
    policy = mkOption {
      description = "The (default) policy to apply to the chain";
      type = types.enum ["accept" "drop"];
      default = "accept";
    };

    priority = mkOption {
      description = "The priority to assign to the chain";
      type = types.str;
      default = "filter";
    };

    prologue = mkOption {
      description = "Rules to insert at the beginning of the chain, before dispatching to other chains happen";
      type = types.lines;
      default = "";
    };

    epilogue = mkOption {
      description = "Rules evaluated after returning from the per-zone chains, when no other verdict was set";
      type = types.lines;
      default = "";
    };

    fragments = fragmentOptions.common // fragmentOptions."${fragmentName}";
  };
in {
  options.networking.zbfw = {
    enable = mkEnableOption "zone-based firewall for nftables";

    zones = mkOption {
      description = "Zone declarations";

      type = with types;
        attrsOf (
          submodule {
            options = {
              interfaces = mkOption {
                description = "Interfaces assigned to this zone";
                type = with types; listOf str;
                default = [];
                example = ["eth0"];
              };

              intraZoneAction = mkOption {
                description = "Default action taken for intra-zone traffic";
                type = types.str;
                default = "accept";
              };

              createStickRules = mkOption {
                description = "Create intra-zone rules for the same interface as in and out at the same time";
                type = types.bool;
                default = false;
              };
            };
          }
        );
      default = {};
    };

    rules.input = mkOption {
      description = "Rules for packets targeted at this system.";
      type = with types; attrsOf (attrsOf str);
      default = {};
      example = {
        lan."10: First rule" = ''
          ct state {established,related} accept
        '';
      };
    };

    rules.forward = mkOption {
      description = "Rules for packets forwarded by this system.";
      type = with types; attrsOf (attrsOf (attrsOf str));
      example = {
        lan.wan."10: First rule" = ''
          counter accept
        '';
      };
      default = {};
    };

    openPorts.tcp = mkOption {
      description = "Open TCP ports (input) for the zone in the map key";
      type = with types; attrsOf (listOf port);
      default = {};
    };

    openPorts.udp = mkOption {
      description = "Open UDP ports (input) for the zone in the map key";
      type = with types; attrsOf (listOf port);
      default = {};
    };

    nftables = {
      tableName = mkOption {
        description = "The table (name) to declare in nftables";
        type = types.str;
        default = "zbfw";
      };

      chains = {
        input = chainModule "input";
        forward = chainModule "forward";
      };

      rpfilter = {
        enable = mkEnableOption' "Enable reverse-path filtering";
      };
    };
  };
}
