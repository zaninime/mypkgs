{
  lib,
  config,
  ...
}:
with lib; let
  cfg = config.networking.zbfw;

  lib' = import ./lib.nix {inherit lib;};

  generated = lib' {inherit cfg;};

  printFragment = config: optionalString config.enable config.rules;
  printFragments = concatMapStringsSep "\n" printFragment;
in {
  config = mkIf cfg.enable {
    networking = {
      # We need nftables, obviously
      nftables.enable = mkDefault true;

      # The missing default in the options
      zbfw.nftables.chains.forward.fragments.offload.rules = mkIf cfg.nftables.chains.forward.fragments.offload.enable (mkDefault ''
        meta l4proto { tcp, udp } flow add @${cfg.nftables.chains.forward.fragments.offload.flowtableName} counter comment "offload established flows"
      '');

      # The actual nftables config
      nftables.tables."${cfg.nftables.tableName}" = {
        family = "inet";
        content = ''
          chain input {
            type filter hook input priority ${cfg.nftables.chains.input.priority}; policy ${cfg.nftables.chains.input.policy};

            ${printFragments (with cfg.nftables.chains.input.fragments; [statefulTreatment acceptLoopback ipv6Neighbors])}

            # prologue
            ${cfg.nftables.chains.input.prologue}

            # dispatch
            ${generated.dispatchingInputRule}

            # epilogue
            ${cfg.nftables.chains.input.epilogue}
          }

          chain forward {
            type filter hook forward priority ${cfg.nftables.chains.forward.priority}; policy ${cfg.nftables.chains.forward.policy};

            ${printFragments (with cfg.nftables.chains.forward.fragments; [offload statefulTreatment acceptDNat])}

            # prologue
            ${cfg.nftables.chains.forward.prologue}

            # dispatch
            ${generated.dispatchingForwardRule}

            # epilogue
            ${cfg.nftables.chains.forward.epilogue}
          }

          ${generated.auxiliaryDeclarations}

          ${optionalString cfg.nftables.rpfilter.enable ''
            chain rpf {
              type filter hook prerouting priority mangle + 10; policy drop;
              meta nfproto ipv4 udp sport . udp dport { 67 . 68, 68 . 67 } accept comment "DHCPv4 client/server"
              meta nfproto ipv6 udp sport . udp dport { 546 . 547, 547 . 546 } accept comment "DHCPv6 client/server"
              fib saddr . mark . iif oif exists accept comment "packets on an interface with a matching FIB entry"
              counter drop
            }
          ''}
        '';
      };
    };

    assertions = with generated.problems; [
      {
        assertion = rulesWithConflictingPriorities == [];
        message = ''
          ZBFW: Multiple rules found with conflicting priorities:
          ${
            concatMapStringsSep "\n" (
              {name, ...}: "  - ${name}"
            )
            rulesWithConflictingPriorities
          }'';
      }
      {
        assertion = undefinedZones == [];
        message = ''
          ZBFW: Zones used in rules don't match the declared ones.
            Undefined zones: ${concatStringsSep ", " undefinedZones}
        '';
      }
      {
        assertion = interfacesInMultipleZones == {};
        message = ''
          ZBFW: One or more interfaces are assigned to multiple zones:
          ${
            concatStringsSep "\n" (
              mapAttrsToList
              (n: v: "  - ${n} assigned to ${concatStringsSep " and " v}")
              interfacesInMultipleZones
            )
          }
        '';
      }
      {
        assertion = reservedZoneNames == [];
        message = ''
          ZBFW: One or more zones are named using a reserved name.
            Names in use: ${concatStringsSep ", " reservedZoneNames}
        '';
      }
    ];

    warnings = optional (generated.problems.emptyZones != []) ''
      ZBFW: One or more zones are empty (no interfaces assigned), but appear in rules.
        Empty zones: ${concatStringsSep ", " generated.problems.emptyZones}
    '';
  };
}
