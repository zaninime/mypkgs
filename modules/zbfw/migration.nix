{lib, ...}: let
  oldPrefix = ["custom" "zfw"];
  newPrefix = ["networking" "zbfw"];
  mkRenamedOptionModule' = old: new: lib.mkRenamedOptionModule (oldPrefix ++ old) (newPrefix ++ new);
  mkRemovedOptionModule' = xs:
    lib.mkRemovedOptionModule (oldPrefix ++ xs) ''
      This options has been removed as the linking into nftables now happens automatically.
    '';
in {
  imports = [
    # exposed
    (mkRenamedOptionModule' ["zones"] ["zones"])
    (mkRenamedOptionModule' ["rules" "input"] ["rules" "input"])
    (mkRenamedOptionModule' ["rules" "forward"] ["rules" "forward"])
    (mkRenamedOptionModule' ["openPorts" "tcp"] ["openPorts" "tcp"])
    (mkRenamedOptionModule' ["openPorts" "udp"] ["openPorts" "udp"])

    # previously "exposed" to link into custom firewall
    (mkRemovedOptionModule' ["generated" "scripts" "inputChains"])
    (mkRemovedOptionModule' ["generated" "scripts" "forwardChains"])
  ];
}
